package com.prueba.put_extra;

import com.prueba.prueba_bundle.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.TextureView;
import android.widget.TextView;

public class RecibeB extends Activity{
	
	public TextView imprimiendoText;
	public TextView imprimiendoText2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recibe_bundle);
		
		imprimiendoText = (TextView) findViewById (R.id.textView1);
		imprimiendoText2 = (TextView) findViewById(R.id.textView2);
		
		Bundle b = new Bundle();
		b = getIntent().getExtras();
		String datos = b.getString("dato0");
		
		imprimiendoText.setText("Dato0: " + datos);
		
	}

}
